
var gulp = require("gulp"),
    sass = require('gulp-sass')(require('sass')),
    postcss = require("gulp-postcss"),
    autoprefixer = require("autoprefixer"),
    cssnano = require("cssnano"),
    sourcemaps = require("gulp-sourcemaps"),
    notify = require("gulp-notify"),
    uglify = require("gulp-uglify"),
    jshint = require("gulp-jshint"),
    browserify = require('browserify'),
    source = require('vinyl-source-stream'),
    buffer = require('vinyl-buffer'),
    babel = require('babelify'),
    del = require('del'),
    browserSync = require("browser-sync").create();

var plugin_paths = {
    sass: [
        'node_modules/'
    ],
    javascript: [
        'node_modules/'
    ],
    pkg: [
        '**/*',
        '!**/node_modules/**',
        '!**/components/**',
        '!**/scss/**',
        '!**/bower.json',
        '!**/gulpfile.js',
        '!**/package.json',
        '!**/composer.json',
        '!**/composer.lock',
        '!**/codesniffer.ruleset.xml',
        '!**/packaged/*',
    ]
};

function reload() {
    browserSync.reload();
}

function styles() {
    return gulp
        .src('src/scss/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({
            includePaths: plugin_paths.sass
        }))
        .on("error", sass.logError)
        .pipe(postcss([autoprefixer(), cssnano()]))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('build/css'))
        .pipe(browserSync.stream());
}

var scripts = gulp.series(lint, js);
gulp.task('scripts', scripts);
function lint() {
    return gulp.src('src/js/*.js')
        .pipe(jshint())
        .pipe(notify(function (file) {
        if (file.jshint.success) {
            return false;
        }
        var errors = file.jshint.results.map(function (data) {
        if (data.error) {
          return "(" + data.error.line + ':' + data.error.character + ') ' + data.error.reason;
        }
        }).join("\n");
        return file.relative + " (" + file.jshint.results.length + " errors)\n" + errors;
    }));
}
function js() {
    var js_uglify = uglify()
        .on('error', notify.onError({
            message: "<%= error.message %>",
            title: "Uglify JS Error"
        }));
    return browserify({
        entries: 'src/js/scripts.js',
        paths: plugin_paths.javascript
    })
    .transform("babelify", {presets: ["@babel/preset-env"]})
    .bundle()
    .on('error', notify.onError({
        message: "<%= error.message %>",
        title: "JS Error"
    }))
    .pipe(source('scripts.js'))
    .pipe(buffer())
    .pipe(js_uglify)
    .pipe(sourcemaps.init())
    .pipe(gulp.dest('build/js'))
    .pipe(browserSync.stream());
}

var clean = gulp.parallel(clean_styles, clean_scripts);
gulp.task('clean', clean);
function clean_styles() {
    return del([
        'css/**'
    ]);
}
function clean_scripts() {
    return del([
        'js/**'
    ]);
}

function watch() {
    browserSync.init( {
        proxy: 'localhost/EvaluatorDrag/build'
    });
    gulp.watch('src/scss/*.scss', gulp.series(clean_styles, styles)).on('change', function(path) {console.log(`File ${path} was changed`)});
    gulp.watch('src/scss/*.scss').on('change', reload);
    gulp.watch('src/js/*.js', gulp.series(clean_scripts, scripts)).on('change', function(path) {console.log(`File ${path} was changed`)});
    gulp.watch('src/js/*.js').on('change', reload);
}

var build = gulp.series(clean, styles, scripts, watch);

gulp.task('default', build);




