
// jQuery
const $ = global.$ = global.jQuery = require('jquery');

// jQuery UI
require('jquery-ui-dist/jquery-ui.min.js');

// Simulate
require('jquery-simulate/jquery.simulate.js');
require('jquery-simulate-ext/src/jquery.simulate.ext.js');
require('jquery-simulate-ext/src/jquery.simulate.drag-n-drop.js');

function addBorder() {
	if ($(window).width() < 768) {
		var totalStatementHeight = 0;
		$("ul.frames li.active .statement").each(function(index) {
			totalStatementHeight+=$(this).outerHeight() + 12;
		});
		totalStatementHeight-=12;
		var targetsPadding = 16;
		$("ul.frames li.active .snap-targets").css('border-top', totalStatementHeight + 'px solid #E5F2F7');
		$("ul.frames li.active .snap-targets").css('padding-top', targetsPadding + 'px');
	} else {
		$("ul.frames li.active .snap-targets").css('border-top', 0);
		$("ul.frames li.active .snap-targets").css('padding-top', 0);
	}
}
function repositionStatements() {
	$("ul.frames li.active .statement").each(function(index) {
		if(!$(this).hasClass('dropped')) { 
	    	var previousStatements = $(this).prevAll('ul.frames li.active .statement');
	    	var topOffset = 0;
	    	previousStatements.each(function(index) {
	    		topOffset+=$(this).outerHeight() + 12;
	    	});
	    	$(this).css('top', topOffset + 'px');
	    }
	});
}
function checkLeast() {
	if($('ul.frames li.active input[value=1]:checked').length) { 
		console.log('Least selected');
		$('.snap-target[data-drop-zone=1]').removeClass('invalid');
		$('.snap-target[data-drop-zone=1]').children('span').html('Place one');
	} else {
		$('.snap-target[data-drop-zone=1]').addClass('invalid');
		$('.snap-target[data-drop-zone=1]').children('span').html('You must drag one statement into this box to continue');
		return false;
	}
}
function checkMost() {
	if($('ul.frames li.active input[value=7]:checked').length) { 
		console.log('Most selected');
		$('.snap-target[data-drop-zone=7]').removeClass('invalid');
		$('.snap-target[data-drop-zone=7]').children('span').html('Place one');
	} else {
		$('.snap-target[data-drop-zone=7]').addClass('invalid');
		$('.snap-target[data-drop-zone=7]').children('span').html('You must drag one statement into this box to continue');
		return false;
	}
}
function checkDropped() {
	if($('ul.frames li.active .statement').length == $('ul.frames li.active .statement.dropped').length) { 
		console.log('All dropped');
		$('#evaluator div.error').remove();
	} else {
		if(!$('ul.frames li.active div.error').length) {
			$('ul.frames li.active .row .columns').prepend('<div class="error invalid">Each statement needs to be sorted into the scale to continue</div>');
			$('ul.frames li.active .row .columns div.error').focus();
		}
		$('ul.frames li.active .snap-target').each(function(index) {
			if(!$(this).hasClass('has-drop')) {
				$(this).addClass('invalid');
			}
		});
		return false;
	}
}

$(function() {
	// Drag and drop
    $(".statement").draggable({
    	snap: ".snap-target",
    	snapTolerance: 16,
    	snapMode: "inner",
    	containment: "parent",
    	drag: function(event, ui) {
    		$(this).removeClass('dropped');
    	},
    	revert: function() {
			if ($(this).hasClass('drag-revert')) {
				$(this).removeClass('drag-revert');
				return true;
			}
		},
    	stop: function(event, ui) {
    		// console.log('stop dragging');
    		$("ul.frames li.active .statement.dropped").each(function(index) {
    			var dropZone = $(this).attr('data-dropped-on');
    			$(`ul.frames li.active .snap-target[data-drop-zone=${dropZone}]`).addClass('has-drop');
    			$(this).find('select').val(dropZone);
			});
          	$(".snap-target").each(function(index) {
          		if(!$(this).hasClass('has-drop')) {
          			$(this).css("height", 'auto');
          		}
          	});
      		$("ul.frames li.active .statement.dropped").each(function(index) {
				var dropZone = $(this).attr('data-dropped-on');
				var repositionTop = $(`ul.frames li.active .snap-target[data-drop-zone=${dropZone}]`).offset().top - $("#evaluator .main .inner").offset().top;
				var repositionLeft = $(`ul.frames li.active .snap-target[data-drop-zone=${dropZone}]`).offset().left - $("#evaluator .main .inner").offset().left;
				$(this).css("top", repositionTop + 'px');
				$(this).css("left", repositionLeft + 'px');
			});

      		var statementNumber = $(this).data('statement');
			var statementDroppedOn = $(this).attr('data-dropped-on');
			if (typeof statementDroppedOn !== 'undefined' && statementDroppedOn !== false) {
				// console.log(statementNumber + statementDroppedOn);
				$(`ul.frames li.active input[name=statement_${statementNumber}][value="${statementDroppedOn}"]`).prop('checked', true);
			}

			if($('ul.frames li.active .snap-target[data-drop-zone=1]').hasClass('invalid') || $('ul.frames li.active .snap-target[data-drop-zone=7]').hasClass('invalid') || $('#evaluator div.error').length) {
				$('ul.frames li.active .snap-target[data-drop-zone=1]').removeClass('invalid');
				$('ul.frames li.active .snap-target[data-drop-zone=1]').children('span').html('Place one');
				$('ul.frames li.active .snap-target[data-drop-zone=7]').removeClass('invalid');
				$('ul.frames li.active .snap-target[data-drop-zone=7]').children('span').html('Place one');
				$('#evaluator div.error').remove();
				$('ul.frames li.active .snap-target').removeClass('invalid');
			}
      	}
    });
    $(".snap-target").droppable({
		drop: function(event, ui) {
			if($(this).hasClass('has-drop')) { 
				console.log('has drop');
				return $(ui.draggable).addClass('drag-revert');
			} else {
				var targetTop = $(this).offset().top;
				var innerTop = $(this).parents('.inner').offset().top;
				var targetLeft = $(this).offset().left;
				var innerLeft = $(this).parents('.inner').offset().left;
				var newTop = targetTop - innerTop;
				var newLeft = targetLeft - innerLeft;
				// console.log(newTop + ' ' + newLeft);
				var newWidth = $(this).outerWidth();
				var droppedOn = $(this).data('drop-zone');
				$(ui.draggable).addClass('dropped');
				$(ui.draggable).attr('data-dropped-on', droppedOn);
				$(ui.draggable).css("top", newTop + 'px');
				$(ui.draggable).css("left", newLeft + 'px');
				$(ui.draggable).css("width", newWidth + 'px');
				var newHeight = $(ui.draggable).outerHeight();
				$(this).css("height", newHeight + 'px');
				$(this).addClass('has-drop');
			}
		},
		out: function( event, ui ) {
			$(this).removeClass('has-drop');
			$(ui.draggable).removeClass('dropped');
			$(ui.draggable).removeAttr('data-dropped-on');
		}
    });
    addBorder();
    repositionStatements();

    // Selects
    $(".statement select").change(function(e) {
    	var selectedValue = $(this).val();
    	var parentStatement = $(this).parents('.statement');
    	var valueDropZone = $(`ul.frames li.active .snap-target[data-drop-zone=${selectedValue}]`);
    	parentStatement.simulate("drag-n-drop", {dragTarget: valueDropZone});

    	var activeDropZones = [];
		$("ul.frames li.active .statement").each(function(value) {
			var activeDrop = $(this).attr('data-dropped-on');
			if(activeDrop != null) {
				activeDropZones.push(activeDrop);
			}
		});
    	$("ul.frames li.active .statement select").each(function(index) {
    		$('ul.frames li.active .statement select option').removeAttr('disabled');
    		activeDropZones.forEach(function(item) {
			    // console.log(item);
			    $(`ul.frames li.active .statement select option[value=${item}]`).attr('disabled', 'disabled');
			});
    	});
    });

    // Frame nav
    var activeFrame = $('ul.frames li.active').data('frame');
    var title = $('#evaluator .row.intro h1');
    var totalFrames = $('#evaluator').data('frame-total');
    if(activeFrame == 1) {
    	$("#prev").hide();
	    $('ul.frames li .statement select').each(function(index) {
	    	if(!$(this).parents('.active').length) {
	    		$(this).attr('disabled', 'disabled');
	    	} 
	    });
    }
    $(".progress-bar .complete").css('width', ((activeFrame / totalFrames) * 100) + '%');
    $("#next").click(function() {
    	checkLeast();
    	checkMost();
    	checkDropped();
    	if($('ul.frames li.active input[value=1]:checked').length && $('ul.frames li.active input[value=7]:checked').length && $('ul.frames li.active .statement').length == $('ul.frames li.active .statement.dropped').length) { 
	    	$('ul.frames li.active').removeClass('active');
	    	activeFrame+=1;
	    	$(`ul.frames li[data-frame=${activeFrame}]`).addClass('active');
    		addBorder();
	    	repositionStatements();
	    	if(activeFrame != 1) {
		    	$("#prev").show();
		    } else {
		    	$("#prev").hide();
		    }
		    title.children('em').html(activeFrame);
		    $("html, body").animate({ scrollTop: 0 }, "fast");
		    $(".progress-bar .complete").css('width', ((activeFrame / totalFrames) * 100) + '%');
	    	console.log(activeFrame);
	    } else {
	    	$("html, body").animate({ 
	    		scrollTop: $("ul.frames li.active .invalid:first").offset().top - 24 
	    	}, "fast");
	    }
	    $('ul.frames li .statement select').each(function(index) {
	    	if(!$(this).parents('.active').length) {
	    		$(this).attr('disabled', 'disabled');
	    	} else {
	    		$(this).removeAttr('disabled');
	    	}
	    });
	    setTimeout(function() {
            $("#evaluator h1").focus();
        }, 50);
	});
	$("#prev").click(function() {
		$('ul.frames li.active').removeClass('active');
    	activeFrame-=1;
    	$(`ul.frames li[data-frame=${activeFrame}]`).addClass('active');
    	repositionStatements();
    	if(activeFrame != 1) {
	    	$("#prev").show();
	    } else {
	    	$("#prev").hide();
	    }
	    $('ul.frames li .statement select').each(function(index) {
	    	if(!$(this).parents('.active').length) {
	    		$(this).attr('disabled', 'disabled');
	    	} else {
	    		$(this).removeAttr('disabled');
	    	}
	    });
	    title.children('em').html(activeFrame);
	    $("html, body").animate({ scrollTop: 0 }, "fast");
		console.log(activeFrame);
	    setTimeout(function() {
            $("#evaluator h1").focus();
        }, 50);
	});
});
$(window).on('resize', function(){
    addBorder();
    repositionStatements();
    $("ul.frames li.active .statement.dropped").each(function(index) {
		var dropZone = $(this).attr('data-dropped-on');
		$(`ul.frames li.active .snap-target[data-drop-zone=${dropZone}]`).addClass('has-drop');
	});
  	$(".snap-target").each(function(index) {
  		if(!$(this).hasClass('has-drop')) {
  			$(this).css("height", 'auto');
  		}
  	});
	$("ul.frames li.active .statement.dropped").each(function(index) {
		var dropZone = $(this).attr('data-dropped-on');
		var repositionTop = $(`ul.frames li.active .snap-target[data-drop-zone=${dropZone}]`).offset().top - $("#evaluator .main .inner").offset().top;
		var repositionLeft = $(`ul.frames li.active .snap-target[data-drop-zone=${dropZone}]`).offset().left - $("#evaluator .main .inner").offset().left;
		var repositionWidth = $(`ul.frames li.active .snap-target[data-drop-zone=${dropZone}]`).outerWidth();
		$(this).css("top", repositionTop + 'px');
		$(this).css("left", repositionLeft + 'px');
		$(this).css("width", repositionWidth + 'px');
	});
});




